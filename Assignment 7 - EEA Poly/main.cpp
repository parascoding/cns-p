#include <iostream>
#include <vector>

using namespace std;

void mod2(vector<int> &a){
    for(int i = 0; i < a.size(); i++)
        a[i] = (a[i] + 2) % 2;
}
vector<int> poly_divide(vector<int> &a, vector<int> b){
    int n = b.size();
    mod2(a);
    mod2(b);
    vector<int> q = vector<int>(a.size() - n + 1);
    while (a.size() >= n){
        int coef = a.back() / b.back();
        int deg = a.size() - b.size();
        q[deg] = coef;
        for (int i = 0; i < n; i++){
            a[a.size() - 1 - i] -= coef * b[b.size() - 1 - i];
        }
        while (a.size() > 0 && a.back() == 0)
            a.pop_back();
    }
    mod2(a);
    mod2(q);
    return q;
}
vector<int> polyMul(vector<int> a, vector<int> b){
    // mod2(a);
    // mod2(b);
    vector<int> c(a.size() + b.size() - 1, 0);
    for (int i = 0; i < a.size(); i++) {
        for (int j = 0; j < b.size(); j++) {
            c[i+j] ^= a[i] ^ b[j];
        }
    }
    // mod2(c);
    while (c.size() > 0 && c.back() == 0)
            c.pop_back();
    return c;
}
vector<int> polySub(vector<int> a, vector<int> b){
    int n = max(a.size(), b.size());
    vector<int> c(n, 0);
    for (int i = 0; i < a.size(); i++) {
        c[i+n-a.size()] ^= a[i];
    }
    for (int i = 0; i < b.size(); i++) {
        c[i+n-b.size()] ^= b[i];
    }
    mod2(c);
    return c;
}
vector<int> eea(vector<int> a, vector<int> b, vector<int> &x, vector<int> &y){
    // mod2(a);
    // mod2(b);
    vector<int> acpy = a;
    vector<int> bcpy = b;

    cout << "A: ";
    for(auto e : a)
        cout << e << " ";
    cout << endl;
    cout << "B: ";
    for(auto e : b)
        cout << e << " ";
    cout << endl;
    if(b.size() == 0){
        x = {1};
        y = {0};
        return a;
    }
    vector<int> x1, y1;
    vector<int> q = poly_divide(a, b);
    cout << "Q: ";
    for(auto e : q)
        cout << e << " ";
    cout << endl;

    cout << "R: ";
    for(auto e : a)
        cout << e << " ";
    cout << endl;
    vector<int> g = eea(b, a, x1, y1);
    
    x = y1;
    y = polySub(x1, polyMul(y1, q));
    // cout << "A CPY: "; 
    // for(auto e : acpy)
    //     cout << e << " ";
    // cout << endl;
    // cout << "B CPY: "; 
    // for(auto e : bcpy)
    //     cout << e << " ";
    // cout << endl;
    // cout << "X: ";
    // for(auto e : x)
    //     cout << e << " ";
    // cout << endl;
    // cout << "Y: ";
    // for(auto e : y)
    //     cout << e << " ";
    // cout << endl;

    cout << endl;
    mod2(g);
    return g;
}
int main(){
    vector<int> numerator = {1, 1, 0, 1, 1, 0, 0, 0, 1};
    vector<int> denominator = {1, 1, 1, 0, 1, 0, 1};
    vector<int> x, y;
    vector<int> g = eea(numerator, denominator, x, y);
    cout << "G: ";
    for(auto e : g)
        cout << e << " ";
    cout << endl;
    cout << "MI: ";
    for(auto e : x)
        cout << e << " ";
    cout << endl;
    
    
}
