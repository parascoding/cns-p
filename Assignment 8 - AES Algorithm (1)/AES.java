// package aes;
import java.util.*;
import java.io.*;
// import aes.SubByte.java;
public class AES{
    static BufferedReader br;
    static PrintWriter ot;
    public static void main(String args[]) throws IOException{
        br = new BufferedReader(new InputStreamReader(System.in));
        ot = new PrintWriter(System.out);
        subByte = new SubByte();
        mixColumn = new MixColumn();
        polyOperation = new PolyOperation();
        pre();
        char text[] = "0123456789abcdeffedcba9876543210".toCharArray();
        char key[] = "0f1571c947d9e8590cb7add6af7f6798".toCharArray();
        // System.out.println(Integer.toBinaryString(dfd));
        initializeWords(key);
        initializeRoundKey();
        int x = polyOperation.polyMul(0x02, 0x87);
        // System.out.println(convertIntegerToHex(x));
        // System.out.println(Integer.toBinaryString(0x02)+" "+Integer.toBinaryString(0x87)+" "+Integer.toBinaryString(x));
        // printRoundKey();
        // printWords();
        encrypt(text);
        ot.close();
    }
    private static void encrypt(char text[]){
        List<List<Integer>> state = new ArrayList<>();
        for(int i = 0; i < 4; i++)
            state.add(new ArrayList<>());
        int row = 0;
        for(int i = 0; i < text.length; i += 2){
            state.get(row).add(convertHexToInteger(text[i], text[i + 1]));
            row = (row + 1) % 4;
        }
        
        addWords(state, 0);
        for(int i = 1; i < 11; i++){
            subWord2(state);
            shiftRows(state);
            if(i != 10)
                mixColumns(state);
            addWords(state, i);
        }
        printState(state);

    }
    private static void mixColumns(List<List<Integer>> state){
        mixColumn.mixColumns(state);
    }
    private static MixColumn mixColumn;
    private static void shiftRows(List<List<Integer>> state){
        for(int i = 0; i < 4; i++)
            state.set(i, new ArrayList<>(shiftRowsBy(state.get(i), i)));
    }
    private static List<Integer> shiftRowsBy(List<Integer> list, int x){
        List<Integer> ans = new ArrayList<>();
        for(int i = x; i < 4; i++)
            ans.add(list.get(i));
        for(int i = 0; i < x; i++)
            ans.add(list.get(i));
        return ans;
    }
    private static void subWord2(List<List<Integer>> state){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                
                state.get(i).set(j, findSubByte(state.get(i).get(j)));
            }
        }
    }
    private static void initializeRoundKey(){
        roundKeys = new ArrayList<>();
        for(int i = 0; i < 44; i++)
            roundKeys.add(new ArrayList<>());
        for(int i = 0; i < 44; i += 4){
            int x1 = i, y1 = 0, x2 = i, y2 = 0;
            while(x1 < i + 4){
                y1 = 0;
                x2 = i;
                while(y1 < 4){
                    roundKeys.get(x2).add(words.get(x1).get(y1));
                    y1++;
                    x2++;
                }
                x1++;
            }
        }
    }
    private static void addWords(List<List<Integer>> state, int roundNo){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++)
                state.get(i).set(j, state.get(i).get(j) ^ roundKeys.get(roundNo * 4 + i).get(j));
        }
    }
    private static void printState(List<List<Integer>> state){
        for(int i = 0; i < state.size(); i++){
            ot.print("s"+i+": ");
            for(int x : state.get(i))
                ot.print(convertIntegerToHex(x) + " ");
            ot.println();
        }
        ot.println();
    }
    private static int findSubByte(int n){
        return subByte.computeSubByte(n);
    }
    private static SubByte subByte;
    private static void printWords(){
        for(int i = 0; i < words.size(); i++){
            ot.print("w"+i+": ");
            for(int x : words.get(i)){
                ot.print(convertIntegerToHex(x)+" ");
            }
            ot.println();
        }
    }
    private static void printRoundKey(){
        for(int i = 0; i < roundKeys.size(); i++){
            ot.print("rk"+i+": ");
            for(int x : roundKeys.get(i)){
                ot.print(convertIntegerToHex(x)+" ");
            }
            ot.println();
        }
    }
    private static void copyList(List<Integer> x, List<Integer> y){
        for(int z : y)
            x.add(z);
    }
    private static List<Integer> subWord(List<Integer> list){
        List<Integer> ans = new ArrayList<>();
        for(int x : list)
            ans.add(findSubByte(x));
        return ans;
    }
    private static List<Integer> rotWord(List<Integer> list){
        List<Integer> ans = new ArrayList<>();
        for(int i = 1; i < list.size(); i++)
            ans.add(list.get(i));
        ans.add(list.get(0));
        return ans;
    }
    private static void printList(List<Integer> list){
        for(int x : list)
            ot.print(convertIntegerToHex(x)+" ");
        ot.println();
    }
    private static void generateNextwordss(){
        for(int i = 4; i < 44; i++){
            words.add(new ArrayList<>());
            List<Integer> list = new ArrayList<>();
            copyList(list, words.get(i - 1));
            if(i % 4 == 0){
                List<Integer> x = rotWord(words.get(i - 1));
                List<Integer> y = subWord(x);
                y.set(0, y.get(0) ^ rcon.get(i / 4 - 1));
                list = new ArrayList<>(y); 
            } 
            for(int j = 0; j < 4; j++)
                words.get(i).add(words.get(i - 4).get(j) ^ list.get(j));
        }
    }
    private static void initializeWords(char key[]){
        words = new ArrayList<>();
        for(int i = 0; i < key.length; i += 2){
            if(i % 8 == 0)
                words.add(new ArrayList<>());
            words.get(i / 8).add(convertHexToInteger(key[i], key[i + 1]));
        }
        generateNextwordss();
    }
    private static int convertHexToInteger(char x, char y){
        String s = mapHexCharToString.get(x)+mapHexCharToString.get(y);
        return convertHexToInteger(s);
    }
    private static int convertHexToInteger(String s){
        int ans = 0, cur = 1;
        for(int i = s.length() - 1; i > - 1; i--){
            if(s.charAt(i) == '1'){
                ans += cur;
            }
            cur <<= 1;
        }
        return ans;
    }
    private static String convertIntegerToHex(int n){
        StringBuilder prefix = new StringBuilder();
        StringBuilder suffix = new StringBuilder();
        for(int i = 4; i < 8; i++){
            prefix.append((1 & (n >> i)));
        }
        prefix = prefix.reverse();
        for(int i = 0; i < 4; i++){
            suffix.append((1 & (n >> i)));
        }
        suffix = suffix.reverse();
        return ""+mapStringToHexChar.get(prefix.toString())+mapStringToHexChar.get(suffix.toString());
    }
    private static void pre(){
        mapHexCharToString = new HashMap<>();
        mapHexCharToString.put('0', "0000");
        mapHexCharToString.put('1', "0001");
        mapHexCharToString.put('2', "0010");
        mapHexCharToString.put('3', "0011");
        mapHexCharToString.put('4', "0100");
        mapHexCharToString.put('5', "0101");
        mapHexCharToString.put('6', "0110");
        mapHexCharToString.put('7', "0111");
        mapHexCharToString.put('8', "1000");
        mapHexCharToString.put('9', "1001");
        mapHexCharToString.put('a', "1010");
        mapHexCharToString.put('b', "1011");
        mapHexCharToString.put('c', "1100");
        mapHexCharToString.put('d', "1101");
        mapHexCharToString.put('e', "1110");
        mapHexCharToString.put('f', "1111");
        mapStringToHexChar = new HashMap<>();
        mapStringToHexChar.put("0000", '0');
        mapStringToHexChar.put("0001", '1');
        mapStringToHexChar.put("0010", '2');
        mapStringToHexChar.put("0011", '3');
        mapStringToHexChar.put("0100", '4');
        mapStringToHexChar.put("0101", '5');
        mapStringToHexChar.put("0110", '6');
        mapStringToHexChar.put("0111", '7');
        mapStringToHexChar.put("1000", '8');
        mapStringToHexChar.put("1001", '9');
        mapStringToHexChar.put("1010", 'a');
        mapStringToHexChar.put("1011", 'b');
        mapStringToHexChar.put("1100", 'c');
        mapStringToHexChar.put("1101", 'd');
        mapStringToHexChar.put("1110", 'e');
        mapStringToHexChar.put("1111", 'f');
        sBox = new ArrayList<Integer>(Arrays.asList(
            0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
            0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
            0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
            0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
            0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
            0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
            0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
            0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
            0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
            0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
            0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
            0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
            0xBA, 0x78, 0x25, 0x2E,	0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
            0x70, 0x3E, 0xB5, 0x66,	0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
            0xE1, 0xF8, 0x98, 0x11,	0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
            0x8C, 0xA1, 0x89, 0x0D,	0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
        ));
        rcon = new ArrayList<>(Arrays.asList(
            0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36
        ));
        mixColMat = new ArrayList<>(Arrays.asList(
            0x02, 0x03, 0x01, 0x01, 0x01, 0x02, 0x03, 0x01, 0x01, 0x01, 0x02, 0x03, 0x03, 0x01, 0x01, 0x02
        ));
        
    }
    private static List<Integer> mixColMat;
    private static List<List<Integer>> words;
    private static List<List<Integer>> roundKeys;
    private static Map<Character, String> mapHexCharToString;
    private static Map<String, Character> mapStringToHexChar;
    private static List<Integer> sBox;
    private static List<Integer> rcon;
    private static PolyOperation polyOperation;
    
}
// "0f1571c947d9e8590cb7add6af7f6798"
// "0123456789abcdeffedbca9876543210"