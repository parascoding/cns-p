class SubByte{
    private int irreduciblePolynomial = 283; // 100011011 representing x^8+x^4+x^3+x+1
    public int computeSubByte(int n){
        int mi[] = eea(irreduciblePolynomial, n);
        int x = multiplyWithMatix(mat, mi[2]);
        x ^= xorWith;
        return x;
    }
    public int inverseSubByte(int n){
        int x = multiplyWithMatix(invMat, n);
        x ^= invXorWith;
        int mi[] = eea(irreduciblePolynomial, x);
        return mi[2];
    }
    public int[] eea(int a, int b){
        if(b == 0){
            return new int[]{a, 1, 0};
        }
        int divRes[] = polyDiv(a, b);
        int ret[] = eea(b, divRes[1]);
        int g = ret[0], x = ret[2];
        int y = polyAdd(ret[1], polyMul(divRes[0], ret[2]));
        return new int[]{g, x, y};
    }
    private int[] polyDiv(int a, int b){
        int quot = 0, rem = 0;
        int num = a, den = b;
        int round = 0;
        while(degree(num) >= degree(den)){
            int x = degree(num), y = degree(den);
            int temp = den << (x - y);
            quot |= (1 << (x - y));
            num = polyAdd(num, temp);
            den = b;
            rem = num;
        }
        return new int[]{quot, rem};
    }
    private int multiplyWithMatix(int mat[], int n){
        int ans = 0;
        for(int i = 0; i < 8; i++){
            int tempAns = 0;
            for(int j = 0; j < 8; j++){
                if(((1 & (mat[i] >> (8 - j - 1))) & (1 & (n >> j))) == 1){
                    tempAns ^= 1;
                }
            }
            if(tempAns == 1)
                ans |= (1 << i);
        }
        return ans;
    }
    private int polyMul(int a, int b){
        int ans = 0;
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                if((1 & (a >> i)) == 1 && (1 & (b >> j)) == 1){
                    ans ^= (1 << (i + j));
                }
            }
        }
        return ans;
    }
    private int polyAdd(int a, int b){
        return a ^ b;
    }
    private int degree(int n){
        for(int i = 31; i > -1; i--){
            if((1 & (n >> i)) == 1)
                return i;
        }
        return -1;
    }
    private void printPoly(int n){
        for(int i = 0; i < 31; i++){
            if((1 & (n >> i)) == 1){
                System.out.print("x^"+i+"+");
            }
        }
        System.out.println();
    }
    
    private int mat[], invMat[], xorWith, invXorWith;
    private void pre(){
        mat = new int[8];
        mat[0] = 0x8f;
        mat[1] = 0xc7;
        mat[2] = 0xe3;
        mat[3] = 0xf1;
        mat[4] = 0xf8;
        mat[5] = 0x7c;
        mat[6] = 0x3e;
        mat[7] = 0x1f;
        xorWith = 0x63;
        invMat = new int[8];
        invMat[0] = 0x25;
        invMat[1] = 0x92;
        invMat[2] = 0x49;
        invMat[3] = 0xa4;
        invMat[4] = 0x52;
        invMat[5] = 0x29;
        invMat[6] = 0x94;
        invMat[7] = 0x4a;
        invXorWith = 0x05;
    }
    public SubByte(){
        pre();
    }
}