import java.util.*;
class MixColumn{
    public void mixColumns(List<List<Integer>> state){
        List<List<Integer>> tState = new ArrayList<>();
        for(List<Integer> x : state)
            tState.add(new ArrayList<>(x));
        for(int i = 0; i < 4; i++){
            for(int j = 0;  j < 4; j++){
                int ans = 0;
                for(int k = 0; k < 4; k++){
                    ans ^= polyOperation.polyMul(tState.get(k).get(j), mat.get(i).get(k));
                }
                state.get(i).set(j, ans);
            }
        }
    }
    public MixColumn(){
        mat = new ArrayList<>();
        polyOperation = new PolyOperation();
        mat.add(new ArrayList<>(List.of(0x02, 0x03, 0x01, 0x01)));
        mat.add(new ArrayList<>(List.of(0x01, 0x02, 0x03, 0x01)));
        mat.add(new ArrayList<>(List.of(0x01, 0x01, 0x02, 0x03)));
        mat.add(new ArrayList<>(List.of(0x03, 0x01, 0x01, 0x02)));
    }
    private List<List<Integer>> mat;
    private PolyOperation polyOperation;
}