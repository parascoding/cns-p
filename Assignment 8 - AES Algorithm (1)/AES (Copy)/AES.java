import java.util.*;
import java.io.*;
public class AES{

    static BufferedReader br;
    static PrintWriter ot;
    private static SubByte subByte;
    private static List<Integer> sBox;
    private static List<Integer> rcon;
    private static PolyOperation polyOperation;
    private static Util util;
    private static RoundKey roundKey;
    private static List<List<Integer>> state;
    public static void main(String args[]) throws IOException{

        br = new BufferedReader(new InputStreamReader(System.in));
        ot = new PrintWriter(System.out);

        util = new Util();
        subByte = new SubByte();
        mixColumn = new MixColumn();
        polyOperation = new PolyOperation();
        pre();
        char text[] = "0123456789abcdeffedcba9876543210".toCharArray();
        char key[] = "0f1571c947d9e8590cb7add6af7f6798".toCharArray();
        roundKey = new RoundKey(key);
        // System.out.println(Integer.toBinaryString(dfd));
        initializeWords(key);
        initializeRoundKey();
        
        encrypt(text);
        decrypt();
        ot.close();
    }
    private static void decrypt(){
        addWords(state, 10);
        printState(state);
        for(int i = 9; i > -1; i--){
            invShiftRows(state);
            invSubWord2(state);
            addWords(state, i);
                if(i != 10)
            invMixColumns(state);
            ot.println("Round: "+i);
            printState(state);
        }
        printState(state);
    }
    private static void encrypt(char text[]){
        state = new ArrayList<>();
        for(int i = 0; i < 4; i++)
            state.add(new ArrayList<>());
        int row = 0;
        for(int i = 0; i < text.length; i += 2){
            state.get(row).add(util.convertHexToInteger(text[i], text[i + 1]));
            row = (row + 1) % 4;
        }
        printState(state);
        addWords(state, 0);
        for(int i = 1; i < 11; i++){
            subWord2(state);
            shiftRows(state);
            if(i != 10)
                mixColumns(state);
                // printState(state);
            addWords(state, i);
            ot.println("Round: "+i);
            printState(state);
        }
        printState(state);
        ot.println("-------------------------------------------------------------------------------------------");
    }
    private static void mixColumns(List<List<Integer>> state){
        mixColumn.mixColumns(state);
    }
    private static void invMixColumns(List<List<Integer>> state){
        mixColumn.invMixColumn(state);
    }
    private static MixColumn mixColumn;
    private static void invShiftRows(List<List<Integer>> state){
        for(int i = 0; i < 4; i++)
            state.set(i, new ArrayList<>(shiftRowsBy(state.get(i), 4-i)));
    }
    private static void shiftRows(List<List<Integer>> state){
        for(int i = 0; i < 4; i++)
            state.set(i, new ArrayList<>(shiftRowsBy(state.get(i), i)));
    }
    private static List<Integer> shiftRowsBy(List<Integer> list, int x){
        List<Integer> ans = new ArrayList<>();
        for(int i = x; i < 4; i++)
            ans.add(list.get(i));
        for(int i = 0; i < x; i++)
            ans.add(list.get(i));
        return ans;
    }
    private static void invSubWord2(List<List<Integer>> state){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                state.get(i).set(j, findInvSubByte(state.get(i).get(j)));
            }
        }
    }
    private static void subWord2(List<List<Integer>> state){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                
                state.get(i).set(j, findSubByte(state.get(i).get(j)));
            }
        }
    }
    private static List<Integer> subWord(List<Integer> list){
        List<Integer> ans = new ArrayList<>();
        for(int x : list)
            ans.add(findSubByte(x));
        return ans;
    }
    private static void initializeRoundKey(){
        roundKey.roundKeys = new ArrayList<>();
        for(int i = 0; i < 44; i++)
            roundKey.roundKeys.add(new ArrayList<>());
        for(int i = 0; i < 44; i += 4){
            int x1 = i, y1 = 0, x2 = i, y2 = 0;
            while(x1 < i + 4){
                y1 = 0;
                x2 = i;
                while(y1 < 4){
                    roundKey.roundKeys.get(x2).add(roundKey.words.get(x1).get(y1));
                    y1++;
                    x2++;
                }
                x1++;
            }
        }
    }
    private static void addWords(List<List<Integer>> state, int roundNo){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++)
                state.get(i).set(j, state.get(i).get(j) ^ roundKey.roundKeys.get(roundNo * 4 + i).get(j));
        }
    }
    private static void printState(List<List<Integer>> state){
        for(int i = 0; i < state.size(); i++){
            ot.print("s"+i+": ");
            for(int x : state.get(i))
                ot.print(util.convertIntegerToHex(x) + " ");
            ot.println();
        }
        ot.println();
    }
    private static int findSubByte(int n){
        return subByte.computeSubByte(n);
    }
    private static int findInvSubByte(int n){
        return subByte.inverseSubByte(n);
    }
    private static void printWords(){
        for(int i = 0; i < roundKey.words.size(); i++){
            ot.print("w"+i+": ");
            for(int x : roundKey.words.get(i)){
                ot.print(util.convertIntegerToHex(x)+" ");
            }
            ot.println();
        }
    }
    private static void printRoundKey(){
        for(int i = 0; i < roundKey.roundKeys.size(); i++){
            ot.print("rk"+i+": ");
            for(int x : roundKey.roundKeys.get(i)){
                ot.print(util.convertIntegerToHex(x)+" ");
            }
            ot.println();
        }
    }
    private static void copyList(List<Integer> x, List<Integer> y){
        for(int z : y)
            x.add(z);
    }
    private static List<Integer> rotWord(List<Integer> list){
        List<Integer> ans = new ArrayList<>();
        for(int i = 1; i < list.size(); i++)
            ans.add(list.get(i));
        ans.add(list.get(0));
        return ans;
    }
    private static void printList(List<Integer> list){
        for(int x : list)
            ot.print(util.convertIntegerToHex(x)+" ");
        ot.println();
    }
    private static void generateNextwordss(){
        for(int i = 4; i < 44; i++){
            roundKey.words.add(new ArrayList<>());
            List<Integer> list = new ArrayList<>();
            copyList(list, roundKey.words.get(i - 1));
            if(i % 4 == 0){
                List<Integer> x = rotWord(roundKey.words.get(i - 1));
                List<Integer> y = subWord(x);
                y.set(0, y.get(0) ^ rcon.get(i / 4 - 1));
                list = new ArrayList<>(y); 
            } 
            for(int j = 0; j < 4; j++)
                roundKey.words.get(i).add(roundKey.words.get(i - 4).get(j) ^ list.get(j));
        }
    }
    private static void initializeWords(char key[]){
        roundKey.words = new ArrayList<>();
        for(int i = 0; i < key.length; i += 2){
            if(i % 8 == 0)
                roundKey.words.add(new ArrayList<>());
            roundKey.words.get(i / 8).add(util.convertHexToInteger(key[i], key[i + 1]));
        }
        generateNextwordss();
    }
  
    private static void pre(){
        rcon = new ArrayList<>(Arrays.asList(
            0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36
        ));
    }
}
// "0f1571c947d9e8590cb7add6af7f6798"
// "0123456789abcdeffedbca9876543210"