class PolyOperation{
    public int polyMul(int a, int b){
        int ans = 0;
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                if((1 & (a >> i)) == 1 && (1 & (b >> j)) == 1){
                    ans ^= (1 << (i + j));
                }
            }
        }
        ans = polyReduce(ans);
        return ans;
    }
    private void printPoly(int n){
        for(int i = 0; i < 31; i++){
            if((1 & (n >> i)) == 1){
                System.out.print("x^"+i+"+");
            }
        }
        System.out.println();
    }
    
    private int polyReduce(int n){
        int i = 18;
        while(i >= 8){
            if((1 & (n >> i)) == 1){
                int a = i - 7, b = 0x1b;
                a = polyMul(a, b);
                n ^= (1 << i);
                n ^= a;            
            } 
            i--;
        }
        return n;
    }
    private int degree(int n){
        for(int i = 31; i > -1; i--){
            if((1 & (n >> i)) == 1)
                return i;
        }
        return -1;
    }
}