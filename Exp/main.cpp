#include <gmp.h>
#include <bits/stdc++.h>
using namespace std;
int main() {
    // Initialize GMP integers
    mpz_t p, q, n, e, d, m, c;
    mpz_inits(p, q, n, e, d, m, c, NULL);

    // Generate random primes p and q
    gmp_randstate_t state;
    gmp_randinit_mt(state);
    mpz_urandomb(p, state, 128);
    mpz_nextprime(p, p);
    mpz_urandomb(q, state, 127);
    mpz_nextprime(q, q);

    cout << p << endl;
    cout << q << endl;
  
    mpz_mul(p, p, q);
    cout << p << endl;
}
