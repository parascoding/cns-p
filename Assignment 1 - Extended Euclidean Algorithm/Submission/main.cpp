#include<bits/stdc++.h>

using namespace std;

int eea(int a, int b, int &x ,int &y){
    if(b == 0){
        x = 1;
        y = 0;
        return a;
    }
    int x1, y1;
    int gcd = eea(b, a % b, x1, y1);
    x = y1;
    y = x1 - y1 * (a / b);
    return gcd;
}
int main(){
    cout << "Enter any positive A and B\n";
    int a, b;
    cin >> a >> b;

    int x, y;
    // eea = Extended Euclidean's Algorithm
    int gcd = eea(a, b, x, y);

    cout << "GCD(" << a << ", " << b <<") = " << gcd;
    cout << "\nAlso, " << a << "*" << x << " + " << b << "*" << y << " = " << gcd;
}
