#include <bits/stdc++.h>
#include <gmp.h>
#include <gmpxx.h>
using namespace std;
using namespace std::chrono;
mpz_t q, aplha, temp;
gmp_randstate_t state;

void find_primitive_root(mpz_t result, mpz_t p) {
    // calculate phi(p)
    mpz_t phi;
    mpz_init(phi);
    mpz_sub_ui(phi, p, 1);

    // factorize phi(p)
    vector<mpz_t> factors;
    mpz_t q;
    mpz_init(q);
    mpz_tdiv_q_ui(q, phi, 2);
    while (mpz_divisible_ui_p(phi, 2)) {
        factors.push_back(q);
        mpz_tdiv_q_ui(phi, phi, 2);
        mpz_set(q, phi);
    }
    factors.push_back(phi);

    // check if each number from 2 to p-1 is a primitive root
    mpz_t g, a, res;
    mpz_init(g);
    mpz_init(a);
    mpz_init(res);
    for (mpz_set_ui(g, 2); mpz_cmp(g, p) < 0; mpz_add_ui(g, g, 1)) {
        bool is_primitive_root = true;
        for (size_t i = 0; i < factors.size(); i++) {
            mpz_powm(a, g, factors[i], p);
            if (mpz_cmp_ui(a, 1) == 0) {
                is_primitive_root = false;
                break;
            }
        }
        if (is_primitive_root) {
            mpz_set(result, g);
            return;
        }
    }

    // if no primitive root is found, set result to 0
    mpz_set_ui(result, 0);
    cout << "PRIMITIVE ROOT: " << result << endl;
}
void choseQandAlpha()
{
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, time(0));
    mpz_init(q);
    mpz_init(aplha);

    mpz_rrandomb(temp, state, 10);
    mpz_nextprime(q, temp);
    cout << q << endl;
}
int main()
{
    auto start = chrono::high_resolution_clock::now();
    string s = "paras";

    choseQandAlpha();
    auto end = chrono::high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(end - start);
    cout << "TIME TAKEN: " << (duration.count()) << " us\n";
}