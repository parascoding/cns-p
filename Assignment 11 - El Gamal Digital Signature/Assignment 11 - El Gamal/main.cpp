#include <bits/stdc++.h>
#include<gmpxx.h>
using namespace std;
gmp_randstate_t state;
mpz_t q, alpha, X_a, Y_a, m, q_1;
long long exp(long long a, long long b, long long mod)
{
    long long ans = 1;
    while (b > 0)
    {
        if ((1 & b) == 1)
        {
            ans = (ans * a) % mod;
        }
        a = (a * a) % mod;
        b >>= 1;
    }
    return (ans);
}
long long findPrimitiveRoot(long p)
{
    vector<long long> factor;
    long long phi = p - 1;
    long long n = phi;
    for (long long i = 2; i * i <= n; i++)
    {
        if (n % i == 0)
        {
            factor.push_back(i);
            while (n % i == 0)
                n /= i;
        }
    }
    if (n > 1)
        factor.push_back(n);
    for (long long ans = 2; ans < p; ans++)
    {
        bool flag = true;
        for (long long fact : factor)
        {
            long long val = exp(ans, phi / fact, p);
            if (val == 1)
            {
                flag = false;
                break;
            }
        }
        if (flag)
            return ans;
    }
    return -1;
}
void choseQandAlpha()
{
    mpz_init(q);
    mpz_init(alpha);
    mpz_set_str(q, "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1"
                   "29024E088A67CC74020BBEA63B139B22514A08798E3404DD"
                   "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245"
                   "E485B576625E7EC6F44C42E9A63A3620FFFFFFFFFFFFFFFF", 16);
    mpz_set_ui(alpha, 2);
    mpz_init(q_1);
    mpz_sub_ui(q_1, q, 1);

}
void generatePublicAndPrivateKey(){
    mpz_init(X_a);
    mpz_init(Y_a);
    mpz_urandomm(X_a, state, q);
    mpz_powm(Y_a, alpha, X_a, q);
    
}
void signMessage(mpz_t m){
    mpz_t K, tempGCD;
    mpz_init(K);
    
    mpz_init(tempGCD);
    do {
        mpz_urandomm(K, state, q_1);
        mpz_gcd(tempGCD, K, q_1);
    } while (mpz_cmp_ui(K, 1) <= 0 || mpz_cmp_ui(tempGCD, 1) != 0);
    
}
void solve(){
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, time(0));
    choseQandAlpha();
    generatePublicAndPrivateKey();

    mpz_init(m);
    mpz_urandomm(m, state, q);

    signMessage(m);
    
}
int main()
{
    srand(time(0));
    solve();
    return 0;
}