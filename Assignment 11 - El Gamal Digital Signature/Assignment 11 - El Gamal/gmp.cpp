#include <bits/stdc++.h>
#include <gmpxx.h>
using namespace std;
void solve()
{
    mpz_t a, b, c;
    mpz_init(a);
    mpz_init(b);
    mpz_init(c);
    gmp_randstate_t state;
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, time(0));
    
    mpz_urandomb(a, state, 128);
    
}
int main()
{
    solve();
    return 0;
}