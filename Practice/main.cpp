#include<bits/stdc++.h>
#include<gmpxx.h>
using namespace std;
mpz_class p, q, N, e, d, M, phi, C;
gmp_randstate_t state;
void initialize(){
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, time(0));
    mpz_init(q.get_mpz_t());
    mpz_init(p.get_mpz_t());
    mpz_init(N.get_mpz_t());
    mpz_init(e.get_mpz_t());
    mpz_init(d.get_mpz_t());
    mpz_init(M.get_mpz_t());
    mpz_init(phi.get_mpz_t());
    mpz_init(C.get_mpz_t());
}

void choseKeys(){
    mpz_rrandomb(p.get_mpz_t(), state, 1024);
    mpz_rrandomb(q.get_mpz_t(), state, 1024);
    mpz_nextprime(p.get_mpz_t(), p.get_mpz_t());
    mpz_nextprime(q.get_mpz_t(), q.get_mpz_t());
    N = p * q;
    phi = (p - 1) * (q - 1);
    mpz_class gcd;
    mpz_init(gcd.get_mpz_t());
    do{
        mpz_urandomm(e.get_mpz_t(), state, phi.get_mpz_t());
        mpz_gcd(gcd.get_mpz_t(), e.get_mpz_t(), phi.get_mpz_t());
    } while(gcd > 1);
    mpz_invert(d.get_mpz_t(), e.get_mpz_t(), phi.get_mpz_t());
}
void encrypt(string message){
    mpz_set_str(C.get_mpz_t(), message.c_str(), 10);
    // cout << "p: " << p << endl;
    // cout << "q: " << q << endl;
    // cout << "N: " << N << endl;
    // cout << "phi: " << phi << endl;
    // cout << "C: " << C << endl;
    // cout << "e: " << e << endl;
    // cout << "d: " << d << endl;
    mpz_powm(C.get_mpz_t(), C.get_mpz_t(), e.get_mpz_t(), N.get_mpz_t());
}
void decrypt(){
    mpz_powm(C.get_mpz_t(), C.get_mpz_t(), d.get_mpz_t(), N.get_mpz_t());
    cout << "DECRYPT: " << C << endl;
}
int main(){
    initialize();
    choseKeys();
    string message = "1234567890987654321234567890987654321";
    encrypt(message);
    decrypt();

}