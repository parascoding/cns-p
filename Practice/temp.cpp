#include <bits/stdc++.h>
#include <gmpxx.h>

using namespace std;
void generate_key(mpz_t p, mpz_t g, mpz_t x, mpz_t y, gmp_randstate_t state) {
    mpz_t q, temp;
    mpz_inits(q, temp, NULL);

    // Generate a random prime number p of around 1024 bits
    mpz_urandomb(p, state, 1024);
    mpz_nextprime(p, p);

    // Choose a random generator g
    mpz_sub_ui(temp, p, 1);
    mpz_divexact_ui(q, temp, 2);
    int count = 0;
    do {
        mpz_urandomm(g, state, p);
        mpz_powm(temp, g, q, p);
        count++;
    } while (mpz_cmp_ui(temp, 1) == 0);
    
    // Choose a random private key x
    mpz_urandomm(x, state, q);

    // Compute public key y
    mpz_powm(y, g, x, p);

    // cout << "P: " << p << endl;
    // cout << "Q: " << q << endl;
    // cout << "Alpha: " << g << endl;
    mpz_clears(q, temp, NULL);
}

void encrypt(mpz_t p, mpz_t g, mpz_t y, mpz_t m, mpz_t c1, mpz_t c2, gmp_randstate_t state) {
    mpz_t k, temp;
    mpz_inits(k, temp, NULL);

    // Choose a random secret key k
    mpz_urandomm(k, state, p);

    // Compute c1 = g^k mod p
    mpz_powm(c1, g, k, p);

    // Compute c2 = m * y^k mod p
    mpz_powm(temp, y, k, p);
    mpz_mul(c2, m, temp);
    mpz_mod(c2, c2, p);

    mpz_clears(k, temp, NULL);
}

void decrypt(mpz_t p, mpz_t x, mpz_t c1, mpz_t c2, mpz_t m) {
    mpz_t temp;
    mpz_init(temp);

    // Compute s = c1^x mod p
    mpz_powm(temp, c1, x, p);

    // Compute m = c2 * s^-1 mod p
    mpz_invert(temp, temp, p);
    mpz_mul(m, c2, temp);
    mpz_mod(m, m, p);

    mpz_clear(temp);
}

int main() {
    mpz_t p, g, x, y, m, c1, c2;
    gmp_randstate_t state;
    gmp_randinit_default(state);
    gmp_randseed_ui(state, time(NULL));

    mpz_inits(p, g, x, y, m, c1, c2, NULL);

    // Generate public and private keys
    generate_key(p, g, x, y, state);

    // Encrypt a message
    mpz_set_str(m, "94559075758604985775", 10);
    encrypt(p, g, y, m, c1, c2, state);

    // Decrypt the ciphertext
    decrypt(p, x, c1, c2, m);

    gmp_printf("Original message: %Zd\n", m);
    gmp_printf("Decrypted message: %Zd\n", m);

    mpz_clears(p, g, x, y, m, c1, c2, NULL);
    gmp_randclear(state);

    return 0;
}
