#pragma once
#include<bits/stdc++.h>
using  namespace std;
unordered_map<char, string> mapHexCharToString = {
    {'0', "0000"},
    {'1', "0001"},
    {'2', "0010"},
    {'3', "0011"},
    {'4', "0100"},
    {'5', "0101"},
    {'6', "0110"},
    {'7', "0111"},
    {'8', "1000"},
    {'9', "1001"},
    {'a', "1010"},
    {'b', "1011"},
    {'c', "1100"},
    {'d', "1101"},
    {'e', "1110"},
    {'f', "1111"}
};

unordered_map<string, char> mapStringToHexChar = {
        {"0000", '0'},
        {"0001", '1'},
        {"0010", '2'},
        {"0011", '3'},
        {"0100", '4'},
        {"0101", '5'},
        {"0110", '6'},
        {"0111", '7'},
        {"1000", '8'},
        {"1001", '9'},
        {"1010", 'a'},
        {"1011", 'b'},
        {"1100", 'c'},
        {"1101", 'd'},
        {"1110", 'e'},
        {"1111", 'f'}
};
int convertHexToInteger1(string s){
    int ans = 0, cur = 1;
    for(int i = s.length() - 1; i > - 1; i--){
        if(s[i] == '1'){
            ans += cur;
        }
        cur <<= 1;
    }
    return ans;
}
string convertIntegerToHex(int n){
    string prefix = "";
    string suffix = "";
    for(int i = 4; i < 8; i++){
        prefix += ((1 & (n >> i)));
    }
    reverse(prefix.begin(), prefix.end());

    for(int i = 0; i < 4; i++){
        suffix += (1 & (n >> i));
    }
    reverse(suffix.begin(), suffix.end());
    return ""+mapStringToHexChar[prefix]+mapStringToHexChar[suffix];
}

int convertHexToInteger(char x, char y){
    string s = mapHexCharToString[x]+mapHexCharToString[y];
    return convertHexToInteger1(s);
}
int main(){
    cout << "Hellp";
}