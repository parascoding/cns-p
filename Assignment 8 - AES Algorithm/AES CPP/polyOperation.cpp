#pragma once
#include<bits/stdc++.h>
using namespace std;

int polyMul(int a, int b);
int polyReduce(int n);

int degree(int n){
    for(int i = 31; i > -1; i--){
        if((1 & (n >> i)) == 1)
            return i;
    }
    return -1;
}

int polyReduce(int n){
    int i = 18;
    while(degree(n) >= 8){
        if((1 & (n >> i)) == 1){
            int a = i - 7, b = 0x1b;
            a = polyMul(1 << a, b);
            n ^= (1 << i);
            n ^= a;            
        } 
        i--;
    }
    return n;
}

int polyMul(int a, int b){
    int ans = 0;
    for(int i = 0; i < 15; i++){
        for(int j = 0; j < 15; j++){
            if((1 & (a >> i)) == 1 && (1 & (b >> j)) == 1){
                ans ^= (1 << (i + j));
            }
        }
    }
    ans = polyReduce(ans);
    return ans;
}


int polyAdd(int a, int b){
    return a ^ b;
}

