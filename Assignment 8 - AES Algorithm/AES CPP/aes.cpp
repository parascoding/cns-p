#pragma once
#include<bits/stdc++.h>
#include "roundKey.hpp"
#include "mixColumn.hpp"
#include "util.hpp"
using namespace std;

vector<vector<int>> roundKey;
vector<vector<int>> state;


void addWords(vector<vector<int>> &state, int roundNo){
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++)
            state[i][j] = state[i][j] ^ roundKey[roundNo * 4 + i][j];
    }
}

void subWord2(vector<vector<int>> &state){
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            state[i][j] = findSubByte(state[i][j]);
        }
    }
}

vector<int> shiftRowsBy(vector<int> list, int x){
        vector<int> ans;
    for(int i = x; i < 4; i++)
        ans.push_back(list[i]);
    for(int i = 0; i < x; i++)
        ans.push_back(list[i]);
    return ans;
}

void shiftRows(vector<vector<int>> &state){
    for(int i = 0; i < 4; i++)
        state[i] = shiftRowsBy(state[i], i);
}

string convertStateToText(vector<vector<int>> state){
    string sb = "";
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            sb += to_string(state[j][i]);
        }
    }
    return sb;
}

void printState(vector<vector<int>> state){
        // if(true)
        //     return;
    for(int i = 0; i < state.size(); i++){
        cout << "s" << i << ": ";
        for(int x : state[i])
            cout << x <<  " ";
        cout << endl;
    }
    cout << "_________________________\n";
}

string encrypt(string text, string key){
    roundKey = getRoundKey(key);
    
    state.clear();
    for(int i = 0; i < 4; i++)
        state.push_back(vector<int>());
    int row = 0;
    for(int i = 0; i < text.length(); i += 2){
        state[row].push_back(convertHexToInteger(text[i], text[i + 1]));
        row = (row + 1) % 4;
    }
    // printState(state);
    addWords(state, 0);
    for(int i = 1; i < 11; i++){
        // cout << "Round: " << i << endl;
        // cout << "Start\n";
        // printState(state);
        subWord2(state);
        // cout << "SubWord\n";
        // printState(state);
        shiftRows(state);
        // cout << "ShiftRows\n";
        // printState(state);
        if(i != 10)
            mixColumns(state);
        // cout << "Mix Colimns\n";
        // printState(state);
        addWords(state, i);
    }
    // printState(state);
    return convertStateToText(state);
}
int main(){
    
    string text = "f2e71dd4a3d4d38540447a6a6ae96758";
    string key = "b2c498b5803fd73320c67ec0ad6c9108";

    string cipher = encrypt(text, key);
    // cout << cipher << endl;
    // for(auto &x : state){
    //     for(auto y : x){
    //         cout << y << " ";
    //     }
    //     cout << endl;
    // }
    // cout << cipher
    // for(auto x : roundKey){
    //     for(auto y : x){
    //         cout << y << " ";
    //     }
    //     cout << endl;
    // }
    return 0;
    
}