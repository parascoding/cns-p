#pragma once
#include<bits/stdc++.h>
#include "polyOperation.hpp"
using namespace std;


vector<vector<int>> matrix = {
    {0x02, 0x03, 0x01, 0x01},
    {0x01, 0x02, 0x03, 0x01},
    {0x01, 0x01, 0x02, 0x03},
    {0x03, 0x01, 0x01, 0x02}
};
void mixColumns(vector<vector<int>> &state){
    vector<vector<int>> tState;
    for(vector<int> x : state)
        tState.push_back(vector<int>(x));
    for(int i = 0; i < 4; i++){
        for(int j = 0;  j < 4; j++){
            int ans = 0;
            for(int k = 0; k < 4; k++){
                ans ^= polyMul(tState[k][j], matrix[i][k]);
            }
            state[i][j] = ans;
        }
    }
}
