import java.util.*;
class Util{
    public Util(){
        mapHexCharToString = new HashMap<>();
        mapStringToHexChar = new HashMap<>();
        fillMapCharToString();
        fillMapStringToHexChar();
    }

    private void fillMapCharToString(){
        mapHexCharToString.put('0', "0000");
        mapHexCharToString.put('1', "0001");
        mapHexCharToString.put('2', "0010");
        mapHexCharToString.put('3', "0011");
        mapHexCharToString.put('4', "0100");
        mapHexCharToString.put('5', "0101");
        mapHexCharToString.put('6', "0110");
        mapHexCharToString.put('7', "0111");
        mapHexCharToString.put('8', "1000");
        mapHexCharToString.put('9', "1001");
        mapHexCharToString.put('a', "1010");
        mapHexCharToString.put('b', "1011");
        mapHexCharToString.put('c', "1100");
        mapHexCharToString.put('d', "1101");
        mapHexCharToString.put('e', "1110");
        mapHexCharToString.put('f', "1111");
    }

    private void fillMapStringToHexChar(){
        mapStringToHexChar.put("0000", '0');
        mapStringToHexChar.put("0001", '1');
        mapStringToHexChar.put("0010", '2');
        mapStringToHexChar.put("0011", '3');
        mapStringToHexChar.put("0100", '4');
        mapStringToHexChar.put("0101", '5');
        mapStringToHexChar.put("0110", '6');
        mapStringToHexChar.put("0111", '7');
        mapStringToHexChar.put("1000", '8');
        mapStringToHexChar.put("1001", '9');
        mapStringToHexChar.put("1010", 'a');
        mapStringToHexChar.put("1011", 'b');
        mapStringToHexChar.put("1100", 'c');
        mapStringToHexChar.put("1101", 'd');
        mapStringToHexChar.put("1110", 'e');
        mapStringToHexChar.put("1111", 'f');
    }
    
    public String convertIntegerToHex(int n){
        StringBuilder prefix = new StringBuilder();
        StringBuilder suffix = new StringBuilder();
        for(int i = 4; i < 8; i++){
            prefix.append((1 & (n >> i)));
        }
        prefix = prefix.reverse();
        for(int i = 0; i < 4; i++){
            suffix.append((1 & (n >> i)));
        }
        suffix = suffix.reverse();
        return ""+mapStringToHexChar.get(prefix.toString())+mapStringToHexChar.get(suffix.toString());
    }
    public int convertHexToInteger(char x, char y){
        String s = mapHexCharToString.get(x)+mapHexCharToString.get(y);
        return convertHexToInteger(s);
    }
    public int convertHexToInteger(String s){
        int ans = 0, cur = 1;
        for(int i = s.length() - 1; i > - 1; i--){
            if(s.charAt(i) == '1'){
                ans += cur;
            }
            cur <<= 1;
        }
        return ans;
    }
    private static Map<Character, String> mapHexCharToString;
    private static Map<String, Character> mapStringToHexChar;
}