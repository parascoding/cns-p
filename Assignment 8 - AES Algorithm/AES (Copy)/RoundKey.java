import java.util.*;
class RoundKey{
    private char key[];
    public List<List<Integer>> words;
    public List<List<Integer>> roundKeys;
    private List<Integer> sBox;
    private List<Integer> rcon;
    private SubByte subByte;
    private Util util;
    public RoundKey(char key[]){
        this.key = key;
        util = new Util();
        subByte = new SubByte();
        rcon = new ArrayList<>(Arrays.asList(
            0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36
        ));
        initializeWords(key);
        initializeRoundKey();
        subByte = new SubByte();
    }
    private void initializeWords(char key[]){
        words = new ArrayList<>();
        for(int i = 0; i < key.length; i += 2){
            if(i % 8 == 0)
                words.add(new ArrayList<>());
            words.get(i / 8).add(util.convertHexToInteger(key[i], key[i + 1]));
        }
        generateNextWords();
    }
    public void generateNextWords(){
        for(int i = 4; i < 44; i++){
            words.add(new ArrayList<>());
            List<Integer> list = new ArrayList<>();
            copyList(list, words.get(i - 1));
            if(i % 4 == 0){
                List<Integer> x = rotWord(words.get(i - 1));
                List<Integer> y = subWord(x);
                y.set(0, y.get(0) ^ rcon.get(i / 4 - 1));
                list = new ArrayList<>(y); 
            } 
            for(int j = 0; j < 4; j++)
                words.get(i).add(words.get(i - 4).get(j) ^ list.get(j));
        }
    }
    private void copyList(List<Integer> x, List<Integer> y){
        for(int z : y)
            x.add(z);
    }
    private List<Integer> subWord(List<Integer> list){
        List<Integer> ans = new ArrayList<>();
        for(int x : list)
            ans.add(findSubByte(x));
        return ans;
    }
    private int findSubByte(int n){
        return subByte.computeSubByte(n);
    }
    public void initializeRoundKey(){
        roundKeys = new ArrayList<>();
        for(int i = 0; i < 44; i++)
            roundKeys.add(new ArrayList<>());
        for(int i = 0; i < 44; i += 4){
            int x1 = i, y1 = 0, x2 = i, y2 = 0;
            while(x1 < i + 4){
                y1 = 0;
                x2 = i;
                while(y1 < 4){
                    roundKeys.get(x2).add(words.get(x1).get(y1));
                    y1++;
                    x2++;
                }
                x1++;
            }
        }
    }
    private static List<Integer> rotWord(List<Integer> list){
        List<Integer> ans = new ArrayList<>();
        for(int i = 1; i < list.size(); i++)
            ans.add(list.get(i));
        ans.add(list.get(0));
        return ans;
    }
}
