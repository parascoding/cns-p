#include<bits/stdc++.h>

using namespace std;


long long exp(long long a, long long b, long long mod){
    long long ans = 1;
    while(b){
        if(b & 1)
            ans = (ans * a) % mod;
        a = (a * a) % mod;
        b >>= 1;
    }
    return ans;
}
bool isComposite(int n, int a, int q, int k){
    long long b = exp(a, q, n);
    if(b == 1) 
        return false;
    for(int j = 0; j < k; j++){
        if(b == n - 1){
            return false;
        }
        b = (b * b) % n;
    }
    return true;
}
bool millerRabin(long long n){
    if(n <= 4){
        return n == 2 || n == 3;
    }
    if(n & 1 == 0)
        return false;
    long long k = 0;
    long long q = n - 1;
    while((q & 1) == 0){
        k++;
        q >>= 1;
    }

    int randomCheck = 5;
    for(int i = 0; i < randomCheck; i++){
        long long a = 2 + rand() % (n - 3);
        
        if(isComposite(n, a, q, k))
            return false;
    }
    
    return true;
}
int main(){
    int t;
    cin >> t;
    while(t-->0){
        long long n;
        cin >> n;
        if(millerRabin(n))
            cout << n << " is Prime\n";
        else
            cout << n << " is Composite\n";
    }
}