import java.util.*;
import java.io.*;
public class Main{
    static BufferedReader br;
    static PrintWriter ot;
    static Scanner sc;
    public static void main(String[] args) throws IOException{
        br = new BufferedReader(new FileReader("input.txt"));
        ot = new PrintWriter(System.out);
        sc = new Scanner(System.in);
        int t = Integer.parseInt(br.readLine().trim());
        while(t-- > 0){
            String s = br.readLine().trim();
            

            ot.println("CIPHERED STRING: "+s);
            decrypt(s);
            ot.println();
        }
        br.close();
        ot.close();
        sc.close();
    }
   
    static void decrypt(String s) throws IOException{
        int freq[][] = new int[26][2];
        for(int i = 0; i < 26; i++)
            freq[i][0] = i;
        for(char c : s.toCharArray()){
            freq[(int)(c - 'a')][1]++;
        }
        Arrays.sort(freq,
            new Comparator<int[]>() {
                public int compare(int x[], int y[]){
                    return y[1] - x[1];
                }
            }
        );
        
        for(int i = 0; i < 25; i++){
            for(int j = i + 1; j < 25; j++){
                int x = freq[i][0];
                int y = freq[j][0];
                int lhs = 15;
                int rhs = y - x;
                int temp[] = eea(lhs, 26);
                if(temp[0] == 1){
                    int a = ((temp[1] * rhs) % mod + mod) % mod;
                    if(eea(a, 26)[0] != 1)
                        continue;
                    int b = ((x - (4 * a)%mod) % mod + mod) % mod;
                    
                    String decipheredString = decrypt(s, a, b);
                    ot.println("DECIPHERED STRING: "+decipheredString);
                    ot.println("ARE YOU HAPPY?\nPress 1 for YES and 0 for NO");
                    ot.flush();
                    int choice = sc.nextInt();
                    if(choice == 1){
                        ot.println("THANK YOU");
                        return;
                    }
                }
                
                
            }
        }
        ot.println("NO MEANINGFUL STRING EXISTS");
    }
    static String decrypt(String s, int a, int b){
        int n = s.length();
        inverse_a = eea(a, 26)[1];
        char deCipheredString[] = new char[n];
        for(int i = 0; i < n; i++){
            char c = s.charAt(i);
            int c_int = (int)(c - 'a');
            int deCiphered_int = (((c_int - b) * inverse_a) % 26 + 26) % 26;
            char deCiphered_char = (char)('a' + deCiphered_int);
            deCipheredString[i] = deCiphered_char;
        }
        return new String(deCipheredString);
    }
    static int[] eea(int a, int b){
        if(b == 0){
            return new int[]{a, 1, 0};
        }

        int ret[] = eea(b, a % b); 
        int g = ret[0];
        int x = ret[2];
        int y = ret[1] - (a / b) * (ret[2]);
        return new int[]{g, x, y};
    }
    static int inverse_a, mod = 26;
}