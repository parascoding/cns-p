import java.util.*;
import java.io.*;
public class Main{
    static BufferedReader br;
    static PrintWriter ot;

    public static void main(String[] args) throws IOException{
        br = new BufferedReader(new FileReader("input.txt"));
        ot = new PrintWriter(System.out);
        int t = Integer.parseInt(br.readLine().trim());
        while(t-- > 0){
            String s = br.readLine().trim();
            s = transform(s);
            String x[] = br.readLine().trim().split(" ");
            int a = Integer.parseInt(x[0]);
            int b = Integer.parseInt(x[1]);
            int g[] = eea(a, 26);
            inverse_a = (g[1] % 26 + 26) % 26;
            ot.println("ORIGINIAL STRING: "+s);
            ot.println("A: "+a+" B: "+b);
            
            String cipheredString = encrypt(s, a, b);
            ot.println("CIPHERED STRING: "+cipheredString);
            String decipheredString = decrypt(cipheredString, a, b);
            ot.println("DECIPHERED STRING: "+decipheredString);
            ot.println();
        }
        br.close();
        ot.close();
    }
    static String encrypt(String s, int a, int b){
        int n = s.length();
        char cipheredString[] = new char[n];
        for(int i = 0; i < n; i++){
            char c = s.charAt(i);
            
            int c_int = (int)(c - 'a');
            int ciphered_int = ((c_int * a) + b) % 26;
            char ciphered_char = (char)('a' + ciphered_int);
            cipheredString[i] = ciphered_char;
        }
        return new String(cipheredString);
    }
    static String decrypt(String s, int a, int b){
        int n = s.length();
        char deCipheredString[] = new char[n];
        for(int i = 0; i < n; i++){
            char c = s.charAt(i);
            int c_int = (int)(c - 'a');
            int deCiphered_int = (((c_int - b) * inverse_a) % 26 + 26) % 26;
            char deCiphered_char = (char)('a' + deCiphered_int);
            deCipheredString[i] = deCiphered_char;
        }
        return new String(deCipheredString);
    }
    static int[] eea(int a, int b){
        if(b == 0){
            return new int[]{a, 1, 0};
        }

        int ret[] = eea(b, a % b); 
        int g = ret[0];
        int x = ret[2];
        int y = ret[1] - (a / b) * (ret[2]);
        return new int[]{g, x, y};
    }
    static String transform(String s){
        s = s.toLowerCase();
        s = s.replaceAll(" ","");
        s = s.replaceAll("\\p{Punct}","");
        return s;
    }
    static int inverse_a;
}