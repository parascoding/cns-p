#include<bits/stdc++.h>
#include<gmpxx.h>
using namespace std;
mpz_class q, alpha, Y_A, temp, X_A, M, C1, C2, k;
gmp_randstate_t state;
void initialize(){
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, time(0));
    mpz_init(q.get_mpz_t());
    mpz_init(alpha.get_mpz_t());
    mpz_init(Y_A.get_mpz_t());
    mpz_init(X_A.get_mpz_t());
    mpz_init(C1.get_mpz_t());
    mpz_init(C2.get_mpz_t());
    mpz_init(k.get_mpz_t());
    mpz_init(M.get_mpz_t());
    mpz_init(temp.get_mpz_t());
}
void choosePublicKey(){
    mpz_rrandomb(q.get_mpz_t(), state, 1024);
    mpz_nextprime(q.get_mpz_t(), q.get_mpz_t());
    mpz_class t;
    mpz_init(t.get_mpz_t());
    t = q - 1;
    t /= 2;
    do{
        mpz_urandomm(alpha.get_mpz_t(), state, q.get_mpz_t());
        mpz_powm(temp.get_mpz_t(), alpha.get_mpz_t(), t.get_mpz_t(), q.get_mpz_t());
    } while(temp == 1);

    cout << "Q: " << q << endl;
    cout << "ALPHA: " << alpha << endl;
}
void choosePrivateKey(){
    mpz_urandomm(X_A.get_mpz_t(), state, q.get_mpz_t());
    mpz_powm(Y_A.get_mpz_t(), alpha.get_mpz_t(), X_A.get_mpz_t(), q.get_mpz_t());
}
void encrypt(string message){
    mpz_urandomm(k.get_mpz_t(), state, q.get_mpz_t());
    mpz_powm(C1.get_mpz_t(), alpha.get_mpz_t(), k.get_mpz_t(), q.get_mpz_t());
    mpz_class K;
    mpz_init(K.get_mpz_t());
    mpz_powm(K.get_mpz_t(), Y_A.get_mpz_t(), k.get_mpz_t(), q.get_mpz_t());
    mpz_set_str(M.get_mpz_t(), message.c_str(), 10);
    M *= K;
    M %= q;
    mpz_set(C2.get_mpz_t(), M.get_mpz_t());
}
void decrypt(){
    mpz_class K;
    mpz_init(K.get_mpz_t());
    mpz_powm(K.get_mpz_t(), C1.get_mpz_t(), X_A.get_mpz_t(), q.get_mpz_t());
    mpz_class Kinv;
    mpz_init(Kinv.get_mpz_t());
    mpz_invert(Kinv.get_mpz_t(), K.get_mpz_t(), q.get_mpz_t());
    C2 *= Kinv;
    C2 %= q;
    cout << "DECRYPT: " << C2;
}
int main(){
    initialize();
    choosePublicKey();
    choosePrivateKey();
    string message = "945590757586049857759140154897";
    encrypt(message);
    decrypt();

}