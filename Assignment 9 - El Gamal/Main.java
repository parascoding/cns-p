import java.util.*;
import java.io.*;
public class Main{
    static BufferedReader br;
    static PrintWriter ot;
    public static void main(String args[]) throws IOException{
        br = new BufferedReader(new InputStreamReader(System.in));
        ot = new PrintWriter(System.out);

        EL_Gamal();

        ot.close();
    }
    static void EL_Gamal(){
        long publicKey[] = generatePublicKey();
        long M = 7;
//	M *= 3;
        ot.println("MESSAGE: "+M);
        ot.println("PUBLIC KEY: ["+publicKey[0]+", "+publicKey[1]+", "+publicKey[2]+"]");
        long k = 1 + (long)(Math.random() * (publicKey[0] - 1));
        long K = exp(publicKey[2], k, publicKey[0]);
        long C1 = exp(publicKey[1], k, publicKey[0]);
        long C2 = (K * M) % publicKey[0];
        ot.println("Cipher: ("+C1+", "+C2+")");

        long t1 = exp(C1, privateKey, publicKey[0]);
        C2 *= 3;
	C2 %= publicKey[0];
        long t2 = (C2 * exp(t1, publicKey[0] - 2, publicKey[0])) % publicKey[0];
        ot.println(t2);
        ot.println("De-Cipher: "+t2);
    }
    static long[] generatePublicKey(){
        long q = generateBigPrimeNumber();
        long alpha = findPrimitiveRoot(q);
        long X_A = 2 + (long)(Math.random() * (q - 2));
        long Y_A = exp(alpha, X_A, q);
        long publicKey[] = new long[]{q, alpha, Y_A};
        privateKey = X_A;
        return publicKey;
    }
    static long exp(long a, long b, long mod){
        long ans = 1;
        while(b > 0){
            if((1 & b) == 1){
                ans = (ans * a) % mod;
            }
            a = (a * a) % mod;
            b >>= 1;
        }
        return (ans);
    }
    static long findPrimitiveRoot(long p){
        List<Long> factor = new ArrayList<>();
        long phi = p - 1;
        long n = phi;
        for(long i = 2; i * i <= n; i++){
            if(n % i == 0){
                factor.add(i);
                while(n % i == 0)
                    n /= i;
            }
        }
        if(n > 1)
            factor.add(n);
        for(long ans = 2; ans < p; ans++){
            boolean flag = true;
            for(long fact : factor){
                long val = exp(ans, phi / fact, p);
                if(val == 1){
                    flag = false;
                    break;
                }
            }
            if(flag)
                return ans;
        }
        return -1;
    }
    static long generateBigPrimeNumber(){
        return (long)(31);
    }
    static long privateKey;
}
