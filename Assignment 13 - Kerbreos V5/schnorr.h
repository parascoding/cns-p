#include <bits/stdc++.h>
#include <gmp.h>
#include <gmpxx.h>
#include <openssl/sha.h>

using namespace std;
using namespace std::chrono;
mpz_class p, q, temp, a, s, v, x, r, e, y;
mpz_class xDash, aPowY, vPowE;
gmp_randstate_t state;
void pre(){
    gmp_randinit_mt(state);
    gmp_randseed_ui(state, time(0));
    mpz_init(q.get_mpz_t());
    mpz_init(p.get_mpz_t());
    mpz_init(temp.get_mpz_t());
    mpz_init(a.get_mpz_t());
    mpz_init(v.get_mpz_t());
    mpz_init(xDash.get_mpz_t());
    mpz_init(aPowY.get_mpz_t());
    mpz_init(vPowE.get_mpz_t());

}

mpz_class generate_random_number(int bitLength){
    mpz_class temp;
    mpz_rrandomb(temp.get_mpz_t(), state, bitLength);
    mpz_nextprime(temp.get_mpz_t(), temp.get_mpz_t());
    return temp;
}

void chosePrivateKey(){
    mpz_class temp;
    mpz_init(temp.get_mpz_t());

    mpz_invert(v.get_mpz_t(), a.get_mpz_t(), p.get_mpz_t());
    mpz_powm(v.get_mpz_t(), v.get_mpz_t(), s.get_mpz_t(), p.get_mpz_t());

}
void chosePublicKey(){
    gmp_randclass rr(gmp_randinit_default);
    rr.seed(time(NULL));
    int QLength = 160, PLength = 1024;
    
    q = 1;
    mpz_mul_2exp(q.get_mpz_t(), q.get_mpz_t(), QLength - 1);
    q += rr.get_z_bits (QLength-2);
    mpz_nextprime(q.get_mpz_t(), q.get_mpz_t());

    p = 1;
    mpz_mul_2exp(p.get_mpz_t(), p.get_mpz_t(), PLength - 1);
    mpz_class n = p / q;
    while(true){
        p = q * n + 1;
        if(mpz_probab_prime_p(p.get_mpz_t(), 25) > 0)
            break;
        n++;
    }

    temp = 2;
    while(true){
        mpz_powm(a.get_mpz_t(), temp.get_mpz_t(), n.get_mpz_t(), p.get_mpz_t());
        if(a > 1)
            break;
        temp++;
    }
    cout << temp << endl;
    mpz_powm(temp.get_mpz_t(), a.get_mpz_t(), q.get_mpz_t(), p.get_mpz_t());
    if(temp != 1){
        cerr << "error";
        exit(-1);
    }
    mpz_urandomm(s.get_mpz_t(), state, q.get_mpz_t());
}


// This I've checked with Merkle Damgard using AES
// as hash function and am getting the same output.
// But since book says SHA-1 should be used as it 
// produces 160-bit output I am using the opnessl
// implementation of it. Kindly Allow. 
mpz_class customHash(string message, mpz_class x){
    message += x.get_str();
    unsigned char digest[SHA_DIGEST_LENGTH];
    SHA1((unsigned char*) message.c_str(), message.length(), digest);
    mpz_class h;
    mpz_init(h.get_mpz_t());
    mpz_import(h.get_mpz_t(), SHA_DIGEST_LENGTH, 1, 1, 0, 0, digest);
    return h;
}
void verify(string message, mpz_class signature){
    mpz_init(y.get_mpz_t());

    mpz_add(y.get_mpz_t(), y.get_mpz_t(), r.get_mpz_t());
    mpz_mod(y.get_mpz_t(), y.get_mpz_t(), q.get_mpz_t());
    mpz_mul(temp.get_mpz_t(), s.get_mpz_t(), e.get_mpz_t());
    mpz_add(y.get_mpz_t(), y.get_mpz_t(), temp.get_mpz_t());
    mpz_mod(y.get_mpz_t(), y.get_mpz_t(), q.get_mpz_t());

  

    mpz_powm(aPowY.get_mpz_t(), a.get_mpz_t(), y.get_mpz_t(), p.get_mpz_t());
    mpz_powm(vPowE.get_mpz_t(), v.get_mpz_t(), e.get_mpz_t(), p.get_mpz_t());
    mpz_mul(xDash.get_mpz_t(), aPowY.get_mpz_t(), vPowE.get_mpz_t());
    mpz_mod(xDash.get_mpz_t(), xDash.get_mpz_t(), p.get_mpz_t());
    
    e = customHash(message, xDash);
    cout << "COMPUTED SIGN:  " << e << endl;

    if(signature == e){
        cout << "VERIFIED\n";
    } else{
        cout << "NOT VERIFIED\n";
    }
}
mpz_class sign(string message){
    pre();
    chosePublicKey();
    chosePrivateKey();
    mpz_init(x.get_mpz_t());    
    mpz_init(r.get_mpz_t());
    mpz_urandomm(r.get_mpz_t(), state, q.get_mpz_t());

    mpz_powm(x.get_mpz_t(), a.get_mpz_t(), r.get_mpz_t(), p.get_mpz_t());
    
    mpz_init(e.get_mpz_t());
    e = customHash(message, x);
    cout << "MESSAGE: " << message << endl;
    cout << "SIGN:\t\t" << e << endl;
    return e;
}

int main(){
    auto start = chrono::high_resolution_clock::now();
    
    string message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dictum aliquet mi, nec aliquam lorem finibus quis. Suspendisse eget efficitur risus. Vivamus mollis nisi vel volutpat sagittis. Donec mattis posuere nulla, ac ullamcorper leo ultricies sed. Nunc sed leo felis. Proin sit amet velit sed nisl imperdiet semper. Fusce mauris sem, laoreet a feugiat at, pretium et mauris. Nullam vulputate nulla justo, ultrices facilisis enim egestas sit amet. Etiam sit amet lorem vulputate, fringilla justo sit amet, scelerisque nunc. Donec suscipit lobortis augue eu fringilla. Curabitur et ornare magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam accumsan a lectus vitae commodo.";
    
    mpz_class signature = sign(message);
    verify(message, signature);

    auto end = chrono::high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(end - start);
    cout << "TIME TAKEN: " << (duration.count()) << " micro seconds\n";
}