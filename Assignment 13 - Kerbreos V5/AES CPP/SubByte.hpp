#pragma once
#include<bits/stdc++.h>
#include "polyOperation.hpp"
using namespace std;

int irreduciblePolynomial = 283; // 100011011 representing x^8+x^4+x^3+x+1

vector<int> mat = {0x8f, 0xc7, 0xe3, 0xf1, 0xf8, 0x7c, 0x3e, 0x1f};
vector<int> invMat = {0x25, 0x92, 0x49, 0xa4, 0x52, 0x29, 0x94, 0x41};
int xorWith = 0x63;
int invXorWith = 0x05;
vector<int> polyDiv(int, int);


vector<int> polyDiv(int a, int b){
    int quot = 0, rem = 0;
    int num = a, den = b;
    int round = 0;
    while(degree(num) >= degree(den)){
        int x = degree(num), y = degree(den);
        int temp = den << (x - y);
        quot |= (1 << (x - y));
        num = polyAdd(num, temp);
        if(round > 10)
            break;
        den = b;
        rem = num;
    }
    return vector<int>{quot, rem};
}
int multiplyWithMatix(vector<int> mat, int n){
    int ans = 0;
    for(int i = 0; i < 8; i++){
        int tempAns = 0;
        for(int j = 0; j < 8; j++){
            if(((1 & (mat[i] >> (8 - j - 1))) & (1 & (n >> j))) == 1){
                tempAns ^= 1;
            }
        }
        if(tempAns == 1)
            ans |= (1 << i);
    }
    return ans;
}


vector<int> eea(int a, int b){
    if(b == 0){
        return vector<int>{a, 1, 0};
    }
    vector<int> divRes = polyDiv(a, b);
    vector<int> ret = eea(b, divRes[1]);
    int g = ret[0], x = ret[2];
    int y = polyAdd(ret[1], polyMul(divRes[0], ret[2]));
    return vector<int>{g, x, y};
}
int computeSubByte(int n){
    vector<int> mi = eea(irreduciblePolynomial, n);
    int x = multiplyWithMatix(mat, mi[2]);
    x ^= xorWith;
    return x;
}
 int inverseSubByte(int n){
    int x = multiplyWithMatix(invMat, n);
    x ^= invXorWith;
    vector<int> mi = eea(irreduciblePolynomial, x);
    return mi[2];
}
