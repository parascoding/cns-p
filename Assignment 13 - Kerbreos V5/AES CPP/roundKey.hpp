#include<bits/stdc++.h>
#include "util.hpp"
#include "SubByte.hpp"

using namespace std;

vector<vector<int>> words;
vector<vector<int>> roundKeys;
vector<int> rcon = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36};

int findSubByte(int n){
    return computeSubByte(n);
}
vector<int> subWord(vector<int> list){
    vector<int> ans;
    for(int x : list)
        ans.push_back(findSubByte(x));
    return ans;
}
vector<int> rotWord(vector<int> list){
    vector<int> ans;
    for(int i = 1; i < list.size(); i++)
        ans.push_back(list[i]);
    ans.push_back(list[0]);
    return ans;
}
void generateNextWords(){
    for(int i = 4; i < 44; i++){
        words.push_back(vector<int>());
        vector<int> list;
        for(int x : words[i - 1])
            list.push_back(x);
        if(i % 4 == 0){
            vector<int> x = rotWord(words[i - 1]);
            vector<int> y = subWord(x);
            y[0] = y[0] ^ rcon[i / 4 - 1];
            list = y; 
        } 
        for(int j = 0; j < 4; j++)
            words[i].push_back(words[i - 4][j] ^ list[j]);
    }
}
void initializeWords(string key){
    words.resize(key.length() / 8, vector<int>());
    for(int i = 0; i < key.length(); i += 2){
        words[i / 8].push_back(convertHexToInteger(key[i], key[i + 1]));
    }
    generateNextWords();
}
void initializeRoundKey(){
    for(int i = 0; i < 44; i++)
        roundKeys.push_back(vector<int>());
    for(int i = 0; i < 44; i += 4){
        int x1 = i, y1 = 0, x2 = i, y2 = 0;
        while(x1 < i + 4){
            y1 = 0;
            x2 = i;
            while(y1 < 4){
                roundKeys[x2].push_back(words[x1][y1]);
                y1++;
                x2++;
            }
            x1++;
        }
    }
}
vector<vector<int>> getRoundKey(string key){
    initializeWords(key);
    initializeRoundKey();
    
    return roundKeys;
}