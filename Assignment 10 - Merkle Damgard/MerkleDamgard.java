import java.io.*;
import java.util.*;

public class MerkleDamgard{
    static BufferedReader br;
    static PrintWriter ot;
    static Util util;
    public static void main(String args[]) throws IOException{
        br = new BufferedReader(new InputStreamReader(System.in));
        ot = new PrintWriter(System.out);
        
        String m = "48692C204920616D2077726974696E672074686973206D65737361676520666F722074686520636F75727365206F66207468652063727970746F677261706879207768696368206973206265696E67207461756768742062792044722E2052204B6162616C6165657368776172616E2073697220696E2074686520496E6469616E20496E73746974757465206F6620496E666F726D6174696F6E20546563686E6F6C6F67792044657369676E20616E64204D616E75666163747572696E67204B75726E6F6F6C2074686174206973206120696E73746974757465206F66206E6174696F6E616C20696D706F7274616E63652E".toLowerCase();
        char h[] = "ffca2f5ff728af22713744c420ef65cd".toCharArray();
        char message[][] = convertMessageToBlocks(m);
        AES aes = new AES();
        util = new Util();
        ot.println("Initial Message (in Blocks):");
        for(int i = 0; i < message.length; i++){
            ot.print("Block "+(i + 1)+": ");
            for(char c : message[i]){
                ot.print(c);
            }
            ot.println();
        }
        ot.println("---------------------------------");
        ot.println("IV: "+new String(h));
        for(int i = 0; i < message.length; i++){
            String temp = AES.aes(message[i], h);
            h = xorString(h, temp.toCharArray());
            ot.println("H("+(i+1)+"): "+new String(h));
        }
        ot.println("Message Digest: "+new String(h));

        ot.close();
    }
    private static char[] xorString(char x[], char y[]){
        char ans[] = new char[x.length];
        for(int i = 0; i < x.length; i++){
            ans[i] = util.xorTwoHexVlaue(x[i], y[i]);
        }
        return ans;
    }
    private static char[][] convertMessageToBlocks(String s){
        s = addPaddingBlock(s);
        int numBlocks = s.length() / 32;
        char ans[][] = new char[numBlocks][32];
        for(int i = 0; i < numBlocks; i++){
            for(int j = 0; j < 32; j++)
                ans[i][j] = s.charAt(i * 32 + j);
        }
        return ans;
    }
    private static String addPaddingBlock(String s){
        int len = s.length();
        int rem = (64 - len) % 32;
        StringBuilder sb = new StringBuilder(s);
        sb.append("1");
        while(rem > 1){
            rem--;
            sb.append("0");
        }

        return sb.toString();
    }
}