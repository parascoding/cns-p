#include<bits/stdc++.h>
using namespace std;

vector<int> pi;

void pre(){
    pi.resize(26);
    for(int i = 0; i < 26; i++){
        pi[i] = i;
    }
    
}
string decrypt(string s, vector<int> invPi){
    string ans = "";
    for(char c : s){
        int ind = (int)(c - 'a');
        ans += (char)('a' + invPi[ind]);
    }
    return ans;
}
vector<int> &getNextKey(){
    int i, j;

    for(i = 24; i > -1; i--)
        if(pi[i] < pi[i + 1])
            break;

    if(i == -1){
        reverse(pi.begin(), pi.end());
        return pi;
    }

    for(j = 25; j > i; j--){
        if(pi[j] > pi[i])
            break;
    }

    swap(pi[i], pi[j]);
    reverse(pi.begin() + i + 1, pi.end());

    return pi;
}
int main(){
    cout << "Enter Cipher Text" << endl; 
    string cipherText;
    cin >> cipherText;
    pre();
    while(true){
        vector<int> key = getNextKey();
        string decipheredText = decrypt(cipherText, key);
        cout << "DE-CIPHERED Text: " << decipheredText << endl;
        cout << "Press 1 if you think it's deciphered otherwsie press other" << endl;
        string x;
        cin >> x;
        if(x == "1")
            break;
    }
    return 0;
}