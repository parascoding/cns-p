#include<bits/stdc++.h>
using namespace std;

vector<int> pi;
vector<int> invPi;
vector<int> pre(){
    pi.resize(26);
    invPi.resize(26);
    for(int i = 0; i < 26; i++)
        pi[i] = i;
    int numOfTimes = (26 * (int)(1e2));
    int times = 0;
    srand(time(0));
    while(times++ < numOfTimes){
        int l = rand() % 26;
        int r = rand() % 26;
        swap(pi[l], pi[r]);
    }
    for(int i = 0; i < 26; i++){
        invPi[pi[i]] = i;
    }
    return pi;
}
string encrypt(string s){
    string ans = "";
    for(char c : s){
        int ind = (int)(c - 'a');
        ans += (char)('a' + pi[ind]);
    }
    return ans;
}

string decrypt(string s){
    string ans = "";
    for(char c : s){
        int ind = (int)(c - 'a');
        ans += (char)('a' + invPi[ind]);
    }
    return ans;
}

int main(){
    string plainText;
    cout << "Enter Plain Text: " << endl;
    cin >> plainText;

    pre();
    
    string cipherText = encrypt(plainText);
    cout << "CIPHER TEXT: " << cipherText << endl;

    string decipheredText = decrypt(cipherText);

    cout << "DE-CIPHER TEXT: " << decipheredText << endl;
}